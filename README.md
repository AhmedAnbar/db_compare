## Installation

Install the package via Composer

`composer require ecms/db-compare`

Install dependencies

`cd vendor/ecms/db-compare && npm install && npm run dev && cd -`

Publish configuration

`php artisan vendor:publish --provider="Ecms\DbCompare\DbCompareServiceProvider"`



## Usage
+ Add database credintials to .env file
+ Run laravel server
```
php artisan serve
```
+ In the browser go to
```
http://127.0.0.1:8000/admin/db-compare/compare-select
```
