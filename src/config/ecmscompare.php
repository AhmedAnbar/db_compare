<?php
$default_connection = env('DB_CONNECTION', 'mysql');
$db = env('DB_DATABASE', 'forge');
return [
  'db' => $db,
  'ignored_db' => [
    'information_schema',
    'performance_schema',
    'mysql'
  ]
];
