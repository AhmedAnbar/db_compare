<?php

namespace Ecms\DbCompare\Http\Controllers;

use App\Http\Controllers\Controller;
use Doctrine\DBAL\DriverManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompareController extends Controller
{
    public function compareList()
    {
      return view('layouts.admin.system.dbcompare.compare_select');
    }
    public function getDatabases()
    {
      $connectionParams = array(
        'dbname' => env('DB_DATABASE'),
        'user' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD', ''),
        'host' => env('DB_HOST'),
        'port' => env('DB_PORT'),
        'charset' => 'utf8',
        'driver' => 'pdo_mysql',
      );
      $conn = DriverManager::getConnection($connectionParams);
      $sm = $conn->getSchemaManager();
      $schema = collect($sm->listDatabases())->map(function ($database, $key) {
        return [
          'name' => $database,
        ];
      });

      foreach ($schema as $key => $sch)
      {
        switch ($sch['name'])
        {
          case 'mysql':
          case 'information_schema':
          case 'performance_schema':
            unset($schema[$key]);
            break;
        }
      }
      return response()->json(['data' => $schema], 200);
    }

    public function compare(Request $request)
    {
      $database1 = new \stdClass();
      $database2 = new \stdClass();
      $data = array();

      $database1->name = $request->database1;
      $database1->tables = array();
      $database2->name = $request->database2;
      $database2->tables = array();
      $connectionParams = array(
        'dbname' => $request->database1,
        'user' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD', ''),
        'host' => env('DB_HOST'),
        'port' => env('DB_PORT'),
        'charset' => 'utf8',
        'driver' => 'pdo_mysql',
      );
      $conn = DriverManager::getConnection($connectionParams);
      $sm = $conn->getSchemaManager();
      $tables = $sm->listTables();
      foreach ($tables as $table)
      {
        $db1Obj = new \stdClass();
        $db1Obj->name = $table->getName();
        $db1Obj->columns = array();

        foreach ($table->getColumns() as $column) {
          $colObj = new \stdClass();
          $colObj->name = $column->getName();
          $colObj->type = $column->getType()->getName();
          $colObj->default = $column->getDefault();
          array_push($db1Obj->columns, $colObj);
        }
        array_push($database1->tables, $db1Obj);
      }

      $connectionParams = array(
        'dbname' => $request->database2,
        'user' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD', ''),
        'host' => env('DB_HOST'),
        'port' => env('DB_PORT'),
        'charset' => 'utf8',
        'driver' => 'pdo_mysql',
      );
      $conn = DriverManager::getConnection($connectionParams);
      $sm = $conn->getSchemaManager();
      $tables = $sm->listTables();

      foreach ($tables as $table)
      {
        $db2Obj = new \stdClass();
        $db2Obj->name = $table->getName();
        $db2Obj->columns = array();

        foreach ($table->getColumns() as $column) {
          $colObj = new \stdClass();
          $colObj->name = $column->getName();
          $colObj->type = $column->getType()->getName();
          $colObj->default = $column->getDefault();
          array_push($db2Obj->columns, $colObj);
        }
        array_push($database2->tables, $db2Obj);
      }
      array_push($data, $database1);
      array_push($data, $database2);
      $this->colorDiff($database1, $database2);
      return response()->json(['data' => $data], 200);
    }

    public function colorDiff($database1, $database2)
    {
      foreach ($database1->tables as $table1)
      {
        foreach ($database2->tables as $table2)
        {
          if ($table1->name === $table2->name)
          {
            $table1->color = '000000';
            $table2->color = '000000';
            $table1->nochange = true;
            $table2->nochange = true;
            foreach ($table1->columns as $column1)
            {
              foreach ($table2->columns as $column2) {
                if ($column1->name === $column2->name)
                {
                  $column1->color = '000000';
                  if ($column1->type === $column2->type)
                  {
                    $column1->colorType = '000000';
                  }
                  if ($column1->default === $column2->default)
                  {
                    $column1->colorDefault = '000000';
                  }
                }
              }
            }

            foreach ($table2->columns as $column2)
            {
              foreach ($table1->columns as $column1) {
                if ($column2->name === $column1->name)
                {
                  $column2->color = '000000';
                  if ($column2->type === $column1->type)
                  {
                    $column2->colorType = '000000';
                  }
                  if ($column2->default === $column1->default)
                  {
                    $column2->colorDefault = '000000';
                  }
                }
              }
            }
          }
        }
      }
    }
}
