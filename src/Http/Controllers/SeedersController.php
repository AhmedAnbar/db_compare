<?php

namespace Ecms\DbCompare\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SeedersController extends Controller
{
  private $database = '';
  private $table = '';
  private $modelNamespace = '';
  private $modelName = '';

  public function __construct(Request $request) {
    $this->database = $request->database;
    $this->table = $request->table;
    $this->modelNamespace = $request->namespace;
    $this->getModelName();
  }
  public function saveSeeder() {
    $tableData = $this->getTableData();
    if($tableData != 'No data') {
      $this->seederTemplate();
      return response()->json(['message' => 'Seeder file created',]);
    } else {
      return response()->json(['message' => 'There is no data in the table'], 400);
    }
  }

  public function getTableData() {
    config(['database.connections.mysql' => [
      'driver' => 'mysql',
      'url' => env('DATABASE_URL'),
      'host' => env('DB_HOST', '127.0.0.1'),
      'port' => env('DB_PORT', '3306'),
      'database' => $this->database,
      'username' => env('DB_USERNAME', 'forge'),
      'password' => env('DB_PASSWORD', ''),
      'unix_socket' => env('DB_SOCKET', ''),
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_unicode_ci',
      'prefix' => '',
      'prefix_indexes' => true,
      'strict' => true,
      'engine' => null,
    ]]);
    $Tabledata = DB::table($this->table)->get();
    if(count($Tabledata) < 1) {
//      dd('There is no data in the table');
      return 'No data';
    } else {
      return $Tabledata;
    }
  }


  public function seederTemplate(): \Illuminate\Http\JsonResponse
  {
    $data = $this->getTableData();
    $schema = "
<?php

use Illuminate\Database\Seeder;

".
"use " .$this->modelNamespace .";"

."

class Ecms" . ucwords($this->table). "Seeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return voidsee
   */
  public function run()
  {

  ";
    foreach ($data as $column) {
    $schema .= $this->modelName . "::create([\n";
      foreach ($column as $key => $value) {
        $schema .= "'" . $key . "' => '" . $value . "',\n";
      }
    $schema .="
    ]);\n";
    }
    $schema .="
  }
}
";

    $this->write($schema, ucwords($this->table));

      return response()->json(['message' => 'file created'], 200);
    }

  public function write($schema, $table)
  {
    $filename = "Ecms" . $table . "Seeder.php";
    $dir = base_path('database/seeds/');
    file_put_contents($dir . "/" . $filename, $schema);
  }

  private function getModelName() {
    $namespace = explode("\\", $this->modelNamespace);
    $model = end($namespace);
    $this->modelName = $model;
  }
}
