<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/db_compare/db_compare.css') }}" rel="stylesheet">
    <title>Compare</title>
</head>
<body>
<div id="app">
  <div class="container-fluid">
    @include('layouts.admin.system.dbcompare.nav')
    @yield('content')
  </div>
</div>
<script type="text/javascript" src="{{ asset('js/db_compare/db_compare.js') }}"></script>
</body>
</html>
