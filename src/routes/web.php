<?php

Route::group(['prefix' => 'admin/db-compare', 'namespace' => 'Ecms\DbCompare\Http\Controllers',], function() {

  Route::get('/compare-select', 'CompareController@compareList')->name('compare.list');
  Route::get('/getdatabases', 'CompareController@getDatabases')->name('get.databases');
  Route::post('/compare', 'CompareController@compare')->name('compare');
  Route::post('/migrate', 'MigrationsController@saveMigration')->name('migrate');
  Route::post('/seeder', 'SeedersController@saveSeeder')->name('seeder');
  Route::post('/updateTable', 'updateDatabaseTableController@update')->name('update.table');

});
