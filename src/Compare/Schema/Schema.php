<?php
namespace Ecms\DbCompare\Compare\Schema;



use Illuminate\Support\Facades\DB;

class Schema
{
  public function tables()
  {
    $ignored_db = config('ecmscompare.ignored_db', []);
    $schema = collect(DB::getDoctrineSchemaManager()->listTableNames())->map(function ($item, $key) {
      return [
        'name' => $item,
        'columns' => DB::getSchemaBuilder()->getColumnListing($item)
      ];
    });
    dd($schema);




//    $query =
//
//      DB::table('information_schema.TABLES')
//      ->select(DB::raw("TABLE_SCHEMA, TABLE_NAME"))
//      ->whereNotIn('TABLE_SCHEMA', $ignored_db)
//      ->whereRaw("TABLE_NAME not like 'diff_%'")
//      ->orderBy('TABLE_SCHEMA', 'ASC')
//      ->orderBy('TABLE_NAME', 'ASC')
//      ->get(['TABLE_SCHEMA', 'TABLE_NAME']);
//
//    return collect($query);
  }
}
